# This file prints a profile by running:
# $ python3 print_profiler_results [FILE_NAME]
# where [FILE_NAME] is the name of the file where you saved your profile

import sys
import pstats
from pstats import SortKey


def main():
  profiling_file = sys.argv[1]
  kuka_update_stats = pstats.Stats(profiling_file)
  kuka_update_stats.sort_stats('cumtime','nfl').print_stats()

if __name__ == '__main__':
  main()