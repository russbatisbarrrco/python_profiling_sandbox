# Import the module with the profiler
from decorator_module import profile_this_function


# This function is going to be profiled with a custom output name:
# output_file = "a_profiled"
@profile_this_function(output_file = "a_profiled")
def a():
  print("this is a(), and is profiled in file \"a_profiled\"!")


# This function is going to be profiled with a default output name:
# output_file = "profiling_default_results"
@profile_this_function(output_file = "a_profiled")
def b():
  print("this is b(), and is profiled in file \"profiling_default_results\"!")


# This function is not decorated, hence not going to be profiled
# no output file
def c():
  print("this is c(), is not decorated and is not profiled")

# This function is not decorated, hence not going to be profiled
# no output file
def d():
  print("this is d(), and is not profiled either")




# Profiling main:
@profile_this_function(output_file = "main_profile")
def main():
  import time
  print("main is sleeping for 1.5\"...")
  time.sleep(1.5)
  print("\nMain has slept\n")
  a()
  b()
  c()

if __name__ == "__main__":
  print("decorator_module_tester.py")
  main()