# This is a decorator module with profiling actions


# Function profiler: Decorate a function with @profile_this_function and you'll get a profiling file in
# folder 'kuka_lwr4_profiling/' with the name of the function that is being profiled, to print you resutls
# run `python3 profile_results.py`, it's recommended to use a redirection since this file may be big, like
# `python3 profile_results.py >> function_name_profiling.txt`


# def profile_this_function(output_file)


def profile_this_function(output_file = 'profiling_default_results'):
    import cProfile, pstats
    def profiler(func):
        def profile_wrapper(*args, **kwargs):
            function_name = func.__name__
            print("Profiling ", function_name)
            with cProfile.Profile() as kuka_update_profiling:
                func(*args, **kwargs)
            kuka_update_stats = pstats.Stats(kuka_update_profiling) 
            print("Saving profile to : ", output_file)
            # kuka_update_stats.dump_stats(output)
            kuka_update_stats.sort_stats('cumtime','nfl').print_stats(30)
            print(function_name, " has been profiled\n")
            # return func(*args, **kwargs)
        return profile_wrapper
    return profiler
