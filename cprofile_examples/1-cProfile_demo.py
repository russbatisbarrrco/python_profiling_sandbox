# https://docs.python.org/3/library/profile.html#instant-user-s-manual
'''
This program is the simpliest example of Pyhton's cProfile, here we import the modules and run
a profiling example with a module name "re", that has a "compile" method.
This method is profiled in a very simple manner and there is also some columns explanations
'''

import cProfile
import re

cProfile.run('re.compile("foo|bar")')

print("\nColumn explanation")

# Description of cProfile output
columns = {
  "ncalls":"Number of calls per function (recursive functions go like a/b)",
  "tottime":"Total time for a single function",
  "percall":"tottime/ncalls",
  "cumtime":"Cumulative time in a function including the time in the child functions",
  "percall":"cumtime/# of primitive(non-recursive) function calls",
  "filename:lineno(function)":"function data"
}

for column_name, description in columns.items():
  print("{:<25}: {}".format(column_name, description))