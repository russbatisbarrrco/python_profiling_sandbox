print("\"profile.Profile\" class: More control than run()/runctx()")

# Part 1: First example
'''
Part 1: this is a little example that works like this:
1 - Initialize and enable a Profiler class instance
2 - Do a piece of code to do some task, in this case just waits a second
3 - Disable the Profiler instance to stop the timer
4 - Print the results
'''


import cProfile, pstats, io
from pstats import SortKey

print("\nFirst example: simple timer")
print("Here we enable a profiler, run code to wait 1\" and then disable the profiler")

# 1 - Initialize and enable a Profiler class instance
my_profiler = cProfile.Profile()
my_profiler.enable()

# 2 - Do a piece of code to do some task, in this case just waits a second
import time
time.sleep(1)

# 3 - Disable the Profiler instance to stop the timer
my_profiler.disable()

# 4 - Print the results
my_stream = io.StringIO()
my_sortby = SortKey.CUMULATIVE

my_ps = pstats.Stats(my_profiler, stream=my_stream).sort_stats(my_sortby)
my_ps.print_stats()
print(my_stream.getvalue())



# Part 2: Second example
'''
Part 1: this is a little example that works by using an instance of the Profile class as a context manager:
1 - Define some dummy functions to be called inside the context management
2 - Creates an instante of Profile and calls the dummy functions inside the context management block
3 - Prints the results
'''


print("\n\nAnother example: Profile as a context manager")

# 1 - Define some dummy functions to be called inside the context management
def wait_a_second():
  time.sleep(1)

def say_hi():
  time.sleep(0.5)
  print("hi")

def sum_x_and_y(x,y):
  time.sleep(0.5)
  return x + y


# 2 - Creates an instante of Profile and calls the dummy functions inside the context management block
with cProfile.Profile() as my_profile_context_manager:
  print("Waiting a second...")
  wait_a_second()
  print("Saying hi...")
  say_hi()
  print("Adding 2 and 5...")
  print(sum_x_and_y(5,2))


# 3 - Prints the results
my_profile_context_manager.print_stats()