'''
In the first lines of this program we create an infinite loop, "dummy_loop()"
that iterates trough "dummy_looped_function()" an is killed by the user with
the keyboard
This is piece of code is going to be profiled to see what is the time it takes
to run "dummy_looped_function()"
'''

import time

def dummy_looped_function():
  time.sleep(0.5)
  print("To exit this loop press \"ctrl-c\"")

def dummy_loop():
  while True:
    try:
      dummy_looped_function()
    except KeyboardInterrupt:
      print ("keyboard broken")
      break



# ------------------------------------------------------------
# ---------------------- Profiling ---------------------------
# ------------------------------------------------------------

import cProfile, pstats


with cProfile.Profile() as my_profiler:
  dummy_loop()


# Creating stats files and printing to screen: 

# my_profiler.print_stats()
my_profiler.dump_stats('cProfile_loop_example_stats')
print("The pstats have been saved to cProfile_loop_example_stats_file")
print("Printing file cProfile_loop_example_stats_file...")
p = pstats.Stats('cProfile_loop_example_stats')
p.print_stats()
