# https://docs.python.org/3/library/profile.html#instant-user-s-manual

'''
This program brings an example of the pstats class usage when profiling with cProfile
The structure is like this:
1 - We define a series of dummy functions to be called and proflied
2 - We make a profiling to be saved in a file called "restats"
3 - Some explanation of pstats mehtods are printed to the user at the moment of executing this program
'''

# 1 - We define a series of dummy functions to be called and proflied:

# ----------------------------------------------------------------------------
# ----------------------------- Dummy functions: -----------------------------
# ----------------------------------------------------------------------------
import time
def a():
  time.sleep(0.125)
  x = 1
  b()
  c()
  d()
  e(0)
  f()

def b():
  time.sleep(0.125)
  y = 2
  c()

def c():
  time.sleep(0.125)
  z = 3
  d()

def d():
  time.sleep(0.125)
  w = 4

def e(x):
  if (2 == x):
    return
  else:
    e(x+1)

def f():
  for x in range(1000):
    g()

def g():
  p = 1
# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------
# ----------------------------------------------------------------------------



# 2 - We make a profiling to be saved in a file called "restats"
import cProfile

cProfile.run('a()', 'restats')

import pstats
from pstats import SortKey
p = pstats.Stats('restats')


print("strip_dirs().sort_stats(-1).print_stats()\n")
p.strip_dirs().sort_stats(-1).print_stats()


print("\nSort by the cumulative time per function (this includes the time of the functions called by a function) considering only the 5 most significant times:\nsort_stats(SortKey.CUMULATIVE).print_stats(5)\n")
p.sort_stats(SortKey.CUMULATIVE).print_stats(5)





# 3 - Some explanation of pstats mehtods are printed to the user at the moment of executing this program
stats_method_names = {
  "strip_dirs()":"Removes extraneous path from all the module names",
  "sort_stats(SortKey.NAME)":"sorts entries",
  "print_stats(NUMBER)":"prints the stats",
}

print("pstats methods:")
for method_name, description in stats_method_names.items():
  print ("{:<20}: {}".format(method_name, description))