import cProfile

# Part 1: run()
'''
Part 1: run()
This part contains an example of how to user the run() method of cProfile and Profile:
1 - We define a string with a command (commands are what are executed by an "exec" in python)
2 - Print some explanation to be seen by the user when the program is ran (you may not read this one here in the code ;) ) 
3 - Runs and print cProfile "run()" function
'''

# 1 - We define a string with a command (commands are what are executed by an "exec" in python)
# Dummy exec for run
my_exec_run = 'print("Hello World")'


# 2 - Print some explanation to be seen by the user when the program is ran
# You may skipp reading thi part here ;) 
print("run() method:")
print("\"profile.run(command, filename, sort)\"\n")
run_args = {
  "run":"Stats instance",
  "command":"an \"exec()\"",
  "What's and \"exec()\"?": "Any execuntable in python, like print()",
  "filename":"Filename to print the Stats, default is to be printed on screen",
  "Sort":"Stats sorting"
}
for run_argument, description in run_args.items():
  print( "{:<20}: {}".format(run_argument, description))



# 3 - Runs and print cProfile "run()" function
print("\nFor this example:")
print("The exec is: {}".format(my_exec_run))
print("Runing: \"cProfile.run(my_exec_run)\"")
cProfile.run(my_exec_run)





# Part 2: run()
'''
Part 2: runctx()
This part contains an example of how to user the runctx() method of cProfile and Profile:
1 - We define a dummy function to be used as an "exec()" argument
2 - Print some explanation to be seen by the user when the program is ran (you may not read this one here in the code ;) ) 
3 - Runs and print cProfile "runctx()" function
'''


# 1 - We define a dummy function to be used as an "exec()" argument
def my_exec_runctx(x,y): 
  return x + y


# 2 - Print some explanation to be seen by the user when the program is ran
# you may not read this one here in the code ;)
print("run() method:")
print("\"profile.run(command, globals, locals, filename, sort)\"\n")

runctx_args = {
  "runctx":"run(), with globals and locals dictionaries for the command string",
  "command":"an \"exec(command, globals, locals)\"",
}

for run_argument, description in runctx_args.items():
  print( "{:<20}: {}".format(run_argument, description))



# 3 - Runs and print cProfile "runctx()" function
print("\nFor this example:")
print("The exec is:")
print("def my_exec_runctx(x,y): ")
print("return x + y\n")

print("Runing:")

my_runctx_string = ''' 
cProfile.runctx(
  "my_exec_runctx(x, y)", 
  {
    "x":1,
    "y":2
  }, 
  locals()
)'''

print(my_runctx_string)

cProfile.runctx(
  "my_exec_runctx(x, y)", 
  {
    "x":1,
    "y":2
  }, 
  locals()
)

print("\nAlso see this examples on runtcx: https://python.hotexamples.com/examples/cProfile/-/runctx/python-runctx-function-examples.html")
# See these examples also: https://python.hotexamples.com/examples/cProfile/-/runctx/python-runctx-function-examples.html