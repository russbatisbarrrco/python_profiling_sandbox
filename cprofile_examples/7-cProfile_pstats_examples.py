'''
This program is a sanbox for Stats class methods.
Here we work again with "re" module
1 - First we explain to the user how Stats work
2 - We create a dummy Profile object to use as argument in Stats
3 - We show the methods of pstats
'''

# 1 - First we explain to the user how Stats work
print("Stats class analyze the data of a profiler:\n")
print("\n\"pstats.Stats(*filenames/profile, stream)\"\n")

print("Arguments explanation:")

# Description of cProfile output
stats_arguments = {
  "*filenames/profile":"a list of filenames or a Profile class object",
  "stream":"a sys.stdout",
}

for stats_argument, description in stats_arguments.items():
  print("{:<20}: {}".format(stats_argument, description))


# 2 - We create a dummy Profile object to use as argument in Stats
import cProfile
import pstats
from dummy_classes_for_pstats import *

with cProfile.Profile() as dummy_profile_object:
  dummy_abcde = my_dummy_abcde()
  dummy_abcde.call_my_dummies_abcde()


# 3 - We show the methods of pstats

from pstats import Stats

print("\nThe \".print_stats()\" just as it is for this example:\n")
dummy_profile_object.print_stats()

# 1) strip_dirs()
print("\n1) \" strip_dirs()\":  removes all leading path information from file names(Stiped info is lost!)\n")
print("*Observation: two functions on the same line of the same filename with the same function name) will be indistinguishable, the statistics are accumulated into a single entry.")

my_stats = pstats.Stats(dummy_profile_object)
my_stats.strip_dirs().print_stats()

# 2) add
print("\n2) add\" \": accumulates additional profiling information into the current profiling object(note that there are added functions in this profiling\n")

# Let's create a new profiling saved to a file:
new_run_to_add_profile = my_dummy_loop()

cProfile.run('new_run_to_add_profile.dummy_loop()', 'dummy_stats_to_add')
my_stats = pstats.Stats(dummy_profile_object)
my_stats.add('dummy_stats_to_add').print_stats()

# 3)  dump_stats(filename)
print("\n3) \"dump_stats(filename)\": Saves you Stats object to a file \"filename\":\n")

print("Saving the \"my_stats\" to a file called \"my_saved_stats\"")
# Let's save my_stats to a file named my_saved_stats
my_stats = pstats.Stats(dummy_profile_object)
my_stats.dump_stats('my_saved_stats')


# Now let's print them to see if it worked!
print("Printing from file \"my_saved_stats\":")
stats_from_file = pstats.Stats('my_saved_stats')
stats_from_file.print_stats()

print("\n\n\"my_saved_stats\" just got printed!")

# 4) sort_stats(*keys)
print("\n4) \"sort_stats(*keys)\":sorting it according to the supplied criteria \n")
print("See the whole list of *keys here: https://docs.python.org/3/library/profile.html#pstats.Stats.sort_stats")
print("\nLet's sort by \"cumtime\", then by \"file\"")

my_stats = pstats.Stats(dummy_profile_object)
my_stats.sort_stats('cumtime', 'file').print_stats()


# 5)  reverse_order()
print("\n5) \"reverse_order()\": reverses the ordering of the basic list within the object, not much more to say\n")

# 6) print_stats(*restrictions)
print("\n6) \"print_stats(*restrictions)\": prints out a report as described in the profile.run() definition, arguments provided (if any) can be used to limit the list down to the significant entries\n")
print("See https://docs.python.org/3/library/profile.html#pstats.Stats.print_stats")


# 7) print_callers(*restrictions)
print("\n7) \"print_callers(*restrictions)\": prints a list of all functions that called each function in the profiled database\n")
my_stats = pstats.Stats(dummy_profile_object)
my_stats.print_callers().print_stats()

# 8) print_callees(*restrictions)
print("\n8) \"print_callees(*restrictions)\": prints a list of all function that were called by the indicated function. \n")
my_stats = pstats.Stats(dummy_profile_object)
my_stats.print_callers().print_stats()

# 9) get_stats_profile()
# print("\n9) \"get_stats_profile()\": eturns an instance of StatsProfile, which contains a mapping of function names to instances of FunctionProfile\n")
# my_stats = pstats.Stats(dummy_profile_object)
# my_stats.print_stats().get_stats_profile()

