'''
This program is to give an example of profiling with "__init__" through different files
This is how it works
1 - in "main()" it creates, instantiaties and runs a dummy method, of a dummy class in a dummy file to do something
2 - Makes the profiling with cProfile of the main method and shows 2 examples of pstats usage
'''


# 1 - Creates, instantiaties and runs a dummy method, of a dummy class in a dummy file to do something
def main():
  from a import a
  from b import b
  from c import c
  from d import d
  from e import e
  a_instance = a()
  b_instance = b()
  c_instance = c()
  d_instance = d()
  e_instance = e()
  a_instance.say_a()
  b_instance.say_b()
  c_instance.say_c()
  d_instance.say_d()
  e_instance.say_e()



# 2 - Makes the profiling with cProfile of the main method and shows 2 examples of pstats usage
if __name__ == "__main__":
  import cProfile
  import pstats
  from pstats import SortKey
  
  cProfile.run('main()','init_profile')
  
  print("sort_stats(SortKey.FILENAME).print_stats('__init__'):\n")
  p = pstats.Stats('init_profile')
  p.sort_stats(SortKey.FILENAME).print_stats('__init__')


  print("sort_stats(SortKey.TIME, SortKey.CUMULATIVE).print_stats(.5, 'init'):\n")
  p.sort_stats(SortKey.TIME, SortKey.CUMULATIVE).print_stats(1.0, 'init')