# Python_profiling_sandbox
This space is for testing profiling in python, it contains documented examples to quickly follow the `cProfile` and `Profile` classes in `Python`.
The easiest way to use it is to see the documentation, follow to the corresponding example in this repository, run the `.py` file, then read the code and play with the code to experiment yourself

## 1. Offical Python documentation's examples
Follow through the folder called `cprofile_examples` with examples from [the official documentation](https://docs.python.org/3/library/profile.html)

### Part 1: [Instant User’s Manual](https://docs.python.org/3/library/profile.html#instant-user-s-manual)
* `1-cProfile_demo.py`: the simplest first example, contains and explanation of the columns
* `2-cProfile_pstats.py`: examples on the pstats objects, with methods like `sort_stats()` over a set of dummy functions to be profiled
* `3-cProfile_init_files/main.py`: is an extension of `2-cProfile_pstats.py` with dummy functions along dummy files and classes, focusing on the `init` argument

### Part 2: [profile and cProfile Module Reference](https://docs.python.org/3/library/profile.html#module-cProfile)
* `4-cProfile_run_runctx.py `: run and runctx explanation (is recommended to take a good look a this and practice)
* `5-cProfile_Profile_class.py `: Using the Profile Class as a mean to gain more control over the execution
* `6-cProfile_loop_example.py `: An example of the Profile class over a loop, this generates and prints a file named `cProfile_loop_example_stats` with the results
### Part 3: [The Stats Class](https://docs.python.org/3/library/profile.html#the-stats-class)
* `7-cProfile_pstats_examples.py `: tackles the Stats object as the mean to analyze Profile objects. Has examples and explanations

### Part 4: [What Is Deterministic Profiling?](https://docs.python.org/3/library/profile.html#what-is-deterministic-profiling)
`cProfile` considers 2 types of profiling:
* *Deterministic*: reflect all function call, return, and exception events are monitored with precise timings for the intervals between these events (code is executing)
* *Statistical profiling*: randomly samples the effective instruction pointer, then deduces where time is being spent (less overhead, but provides only relative indications of where time is being spent)

Python tends to add much overhead to execution, that deterministic profiling tends to only add small processing overhead in typical applications, so is not that expensive, yet provides extensive run time statistics about the execution of a Python program.

Some benefits:
* Call count statistics help identify bugs in code (surprising counts), and inline-expansion points (high call counts). 
* Internal time statistics help identify “hot loops” that should be carefully optimized. 
* Cumulative time statistics should be used to identify high level errors in the selection of algorithms. 
  * An unusual handling of cumulative times in this profiler allows statistics for recursive implementations of algorithms to be directly compared to iterative implementations.


### Part 5: [Limitations](https://docs.python.org/3/library/profile.html#limitations)

1. One limitation: accuracy of timing information, fundamental problem in deterministic profilers involving accuracy. 

   THe “clock” is only ticking at a rate (typically) of about .001 seconds, no measurements will be more accurate than the underlying clock. 
   For enough measurements the “error” will tend to average out. 
   Removing this first error induces a second source of error.

2. Second problem: it “takes a while” from when an event is dispatched until the profiler’s call to get the time actually gets the state of the clock. 
   Also a certain lag when exiting the profiler event handler from the time that the clock’s value was obtained (and then squirreled away), until the user’s code is once again executing. 
   Functions that are called many times, or call many functions, will typically accumulate this error. 
   The error that accumulates in this fashion is typically less than the accuracy of the clock (less than one clock tick), but it can accumulate and become very significant.


The problem is more important with profile than with the lower-overhead cProfile. 
After the profiler is calibrated, it will be more accurate, but it will sometimes produce negative numbers.

### Part 6: [Calibration](https://docs.python.org/3/library/profile.html#calibration)

This part is very quick to read and reproduce in the docs: https://docs.python.org/3/library/profile.html#calibration

### Part 7: [Using a custom timer](https://docs.python.org/3/library/profile.html#using-a-custom-timer)
This part is also very quick to read and reproduce in the docs: https://docs.python.org/3/library/profile.html#using-a-custom-timer


## Profiling decorator: An example of how to quickly profile a function by adding 2 lines to your file

You may begin this part by doing this:
```
cd profiling_decorator_example
python3 file_to_test_profiling.py
```

This will generate 3 new files, try them by running, for example:
```
python3 print_profiler_results.py main_profile
```

In the foler `profiling_decorator_example` there is an example to add a custom profilin to a given function in python
The module that does de work is:
```
decorator_module.py
```
A dummy code is present in:
```
file_to_test_profiling.py
```
Here there are 3 decorated functions that are decorated and will be profiled with their own output file, and 2 more that are not gonna be profiled alone.

And finally the file that prints the results is:
```
python3 print_profiler_results.py [PROFILER_FILE_NAME]
```

