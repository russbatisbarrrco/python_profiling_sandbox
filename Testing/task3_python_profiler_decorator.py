def my_decorator(func):
  def wrapper(*args, **kwargs):
    import cProfile
    # cProfile.run('func(*args, **kwargs)', "output.dat")
    from cProfile import Profile
    prof = Profile()
    prof.enable()
    func(*args, **kwargs)
    prof.create_stats()
    prof.print_stats()

    # import pstats
    # from pstats import SortKey
    # with open("output_time.txt", "w") as f:
    #   p = pstats.Stats("output.dat", stream=f)
    #   p.sort_stats("time").print_stats()

    # with open("output_calls.txt", "w") as f:
    #   p = pstats.Stats("output.dat", stream=f)
    #   p.sort_stats("calls").print_stats()
  return wrapper

  
@my_decorator
def my_function():
  import time
  time.sleep(2)
  print("This is my function")


my_function()


