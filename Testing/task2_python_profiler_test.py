# This profiler stores the data in 2 diferent files 

def my_profiling():
  import cProfile
  cProfile.run('my_function()', "output.dat")
  
  import pstats
  from pstats import SortKey

  with open("output_time.txt", "w") as f:
    p = pstats.Stats("output.dat", stream=f)
    p.sort_stats("time").print_stats()

  with open("output_calls.txt", "w") as f:
    p = pstats.Stats("output.dat", stream=f)
    p.sort_stats("calls").print_stats()


def my_function():
  import time
  time.sleep(2)
  print("This is my function")


my_profiling()