# Task: 
# Hacer corrida de un programa sencillo, hecho por vos con cprofiler. Dicho programa que contenga un while loop con 1000 ciclos por ejemplo.

import cProfile

# ------------------------------------------------------------------------------------------------------------ 
# -------------------------- This is a dummy simple run just to accumulate some time -------------------------- 
# ------------------------------------------------------------------------------------------------------------ 

def simple_run():
  for counts in range(1000):
    do_some_simple_stuff()
  print("simple_run is ready")

def do_some_simple_stuff():
  for y in range(100):
    do_other_simple_stuff()

def do_other_simple_stuff():
  x = 0
  for y in range(100):
    x+=y

# ------------------------------------------------------------------------------------------------------------ 
# ------------------------------------------------------------------------------------------------------------ 
# ------------------------------------------------------------------------------------------------------------ 

# ------------------------------------------- This is the profiling ------------------------------------------ 

# Simple terminal-printed profile:
print("A simple run with results in the terminal:\n")
cProfile.run('simple_run()')

# File-printed profile:
print("For a file-saved run see file \"task_Fede_file_results\":\n")
cProfile.run('simple_run()', "task_Fede_file_results")

