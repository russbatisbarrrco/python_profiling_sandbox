def my_cool_decorator(func):
	def wrapper(*args, **kwargs):
		
		print("my_cool_decorator")
		func(*args, **kwargs)
		print("my_cool_decorator again\n")

	return wrapper


@my_cool_decorator
def my_original_function():
	print("my_original_function")


@my_cool_decorator
def my_other_function():
	print("my_other_function")


my_original_function()
my_other_function()
