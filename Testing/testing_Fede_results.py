import pstats
from pstats import SortKey
p = pstats.Stats('kuka_lwr4_profiling/kuka_update_arm_state_profile')
p.strip_dirs().sort_stats(-1).print_stats()
p.sort_stats(SortKey.NAME)


p.print_stats()