
class Kuka_lwr4plus_sim():
  def __init__(self):
    self.update_arm_state()
    
  # Here we are still using a decorator, but not an "@decorator" annotation like
  # We are going to catch update_arm_state funcition and profile it
  def update_arm_state(self):
      import cProfile
      cProfile.runctx('self.update_arm_state_profiled()',globals(), locals(),filename="kuka_lwr4_profiling/kuka_update_arm_state_profile")

  def update_arm_state_profiled(self):
    import time
    time.sleep(2)
    print("simulate the arm")

def main(args=None):
      kuka_lwr4plus_sim = Kuka_lwr4plus_sim()

if __name__ == '__main__':
  main()