import cProfile
import re


# ------------------------------------------- This is the profiling ------------------------------------------ 


def profile_this_execution(func):
  def wrapper(*args, **kwargs):

    print("my_cool_decorator")
    # Simple terminal-printed profile:
    print("A simple run with results in the terminal:\n")
    cProfile.run('func(*args, **kwargs)')

    # File-printed profile:
    # print("For a file-saved run see file \"task_Fede_file_results\":\n")
    # cProfile.run('func(*args, **kwargs)', "task_Fede_file_results")
    print("my_cool_decorator again\n")

  return wrapper

# ------------------------------------------------------------------------------------------------------------ 
# -------------------------- This is a dummy simple run just to accumulate some time -------------------------- 
# ------------------------------------------------------------------------------------------------------------ 

@profile_this_execution
def simple_run():
  for counts in range(1000):
    do_some_simple_stuff()
  print("simple_run is ready")


def do_some_simple_stuff():
  for y in range(100):
    do_other_simple_stuff()

def do_other_simple_stuff():
  x = 0
  for y in range(100):
    x+=y

# ------------------------------------------------------------------------------------------------------------ 
# ------------------------------------------------------------------------------------------------------------ 
# ------------------------------------------------------------------------------------------------------------ 


simple_run()